import {TipoGrafico} from './TipoGrafico.js';
export class Grafico{
  private datos:number[];
  constructor(datos:number[]){
    this.datos=datos;
  }
  public draw(tipo:TipoGrafico):void{
    tipo.draw(this.datos);
  }
}

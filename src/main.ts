import {Grafico} from './Grafico.js';
import {GraficoBarras} from './GraficoBarras.js';
import {GraficoLineas} from './GraficoLineas.js';
import {GraficoTarta} from './GraficoTarta.js';
let grafico:Grafico=new Grafico([100,120,130,70,150,170,80,20,50,90,120]);
grafico.draw(new GraficoBarras(300,200));
grafico.draw(new GraficoLineas(300,200));
grafico.draw(new GraficoTarta(300,200));

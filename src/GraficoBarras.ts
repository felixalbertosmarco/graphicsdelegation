import {TipoGrafico} from './TipoGrafico.js';
export class GraficoBarras extends TipoGrafico{
  public draw(datos:number[]):void{
    super.draw(datos);
    for(let i=0;i<datos.length;i++){
      this.drawChar(datos[i],this.getBaseX(i),this.canvas.height,10);
    }
  }
  public drawChar(x:number,basex:number,basey:number,width:number){
    this.ctx.lineWidth=5;
    this.ctx.strokeRect(basex,basey-x,width,x);
  }
}

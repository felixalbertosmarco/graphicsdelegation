export abstract class TipoGrafico{
  protected canvas:HTMLCanvasElement;
  protected ctx:CanvasRenderingContext2D;
  protected separacion:number;
  protected borde:number;
  constructor(ancho:number,alto:number){
    this.canvas = document.createElement('canvas') as HTMLCanvasElement;
    this.canvas.width=ancho;
    this.canvas.height=alto;
    this.canvas.style.border='solid 2px black';
    document.body.appendChild(this.canvas);
    this.ctx = this.canvas.getContext('2d');
    this.clear();
  }
  public draw(datos:number[]):void{
    this.separacion=this.canvas.width/datos.length;
    this.borde=10;
  }
  protected getBaseX(n:number):number{
    return n*this.separacion+this.borde;
  }
  public clear(){
    this.drawSquare(0,0,this.canvas.width,this.canvas.height,'white');
  }
  public drawSquare(x:number,y:number,w:number,h:number,c:string){
    this.ctx.fillStyle = c;
    this.ctx.fillRect(x,y,w,h);
  }
}

import {TipoGrafico} from './TipoGrafico.js';
export class GraficoLineas extends TipoGrafico{
  public draw(datos:number[]):void{
    super.draw(datos);
    for(let i=0;i<datos.length;i++){
      this.drawPoint(datos[i],this.getBaseX(i),this.canvas.height,5);
      if(i>0){
        this.drawLine(
          this.getBaseX(i-1),this.canvas.height-datos[i-1],
          this.getBaseX(i),this.canvas.height-datos[i]
        );
      }
    }
  }
  public drawPoint(x:number,basex:number,basey:number,width:number){
    this.ctx.beginPath();
    this.ctx.arc(basex,basey-x,width,0,2*Math.PI,false);
    this.ctx.fillStyle='black';
    this.ctx.fill();
    this.ctx.lineWidth=2;
    this.ctx.strokeStyle='red';
    this.ctx.stroke();
  }
  public drawLine(x0:number,y0:number,x1:number,y1:number){
    this.ctx.beginPath();
    this.ctx.moveTo(x0,y0);
    this.ctx.lineTo(x1,y1);
    this.ctx.strokeStyle='black';
    this.ctx.stroke();
  }
}

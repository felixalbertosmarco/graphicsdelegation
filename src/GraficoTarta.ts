import {TipoGrafico} from './TipoGrafico.js';
export class GraficoTarta extends TipoGrafico{
  private total:number;
  private anterior:number;
  private colores:string[];
  constructor(ancho:number,alto:number){
    super(ancho,alto);
    this.colores=['Red','Maroon','Yellow','Olive','Lime','Green','Aqua','Teal','Blue','Navy','Fuchsia','Purple','Silver','Gray','Black'];
  }
  public draw(datos:number[]):void{
    super.draw(datos);
    this.total=0;
    let centroX=this.canvas.width/2;
    let centroY=this.canvas.height/2;
    for(let i=0;i<datos.length;i++){
      this.total+=datos[i];
    }
    this.anterior=0;
    for(let i=0;i<datos.length;i++){
        this.drawTrozo(centroX,centroY,datos[i],this.canvas.height/2,this.colores[i%this.colores.length]);
      this.anterior+=datos[i];
    }
  }
  private drawTrozo(x:number,y:number,valor:number,radio:number,color:string):void{
    this.ctx.beginPath();
    this.ctx.moveTo(x,y);
    this.ctx.arc(x,y,radio,this.getRadio(this.anterior),this.getRadio(this.anterior)+this.getRadio(valor),false);
    this.ctx.fillStyle=color;
    this.ctx.fill();
    this.ctx.strokeStyle='white';
    this.ctx.lineWidth=3;
    this.ctx.moveTo(x,y);
    this.ctx.stroke();
  }
  private getRadio(v:number):number{
    return (v*2*Math.PI)/this.total;
  }
  
}
